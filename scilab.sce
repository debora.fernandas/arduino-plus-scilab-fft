// Copyright (C) Alexander Kazantsev (AlexKaz, alexkazancev@bk.ru)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

global mas_otschety_num  okno_grafiki doloop mas_otschety_size size_fft size_max
global serial_handle display_schetcik size_obnovl

// If this code is usuful for you donate please, https://yoomoney.ru/to/410019721796762


size_obnovl=128; // через сколько отсчётов обновлять графики
size_fft=1024; // количество отсчетов для FFT
size_max=1024*8;
doloop=%F; // выполнять или не выполнять основной цикл
display_schetcik=int(size_fft/size_obnovl)-1;
size_grafik=512;
koef_hz=0.1; // корректирующий коэффициент для оси частот


function grafiki_obnovlenie()
    global mas_otschety_num mas_otschety_size;
    global okno_grafiki display_schetcik;
    
    drawlater();//set("immediate_drawing","off");
    
    x=mas_otschety_size;//size(mas_otschety_num(:,1))(1);
    //disp(display_schetcik);
    //disp(x);
    //disp(int(x/size_obnovl));
    //disp(int(x/size_obnovl)>display_schetcik);
    if (x>size_fft) & (int(x/size_obnovl)>display_schetcik) then
        scf(okno_grafiki); 
        clf();

//    if size(mas_otschety_num(:,1))(1)>size_fft then //& (size(mas_otschety_num(:,1))(1)>size_fft) then

        display_schetcik=int(x/size_obnovl);
        
        [x,y]=correct_fft(mas_otschety_num($-size_fft+1:$,1),1/(1024*8));
            y=from_ddx_to_x(x,y);
              subplot(3,2,1); plot(mas_otschety_num($-size_fft+1:$,1));
              subplot(3,2,2); plot2d(x(4:size_grafik)*koef_hz,y(4:size_grafik));
        [x,y]=correct_fft(mas_otschety_num($-size_fft+1:$,2),1/(1024*8));
            y=from_ddx_to_x(x,y);
              subplot(3,2,3); plot(mas_otschety_num($-size_fft+1:$,2));
              subplot(3,2,4); plot2d(x(4:size_grafik)*koef_hz,y(4:size_grafik));
        [x,y]=correct_fft(mas_otschety_num($-size_fft+1:$,3),1/(1024*8));
            y=from_ddx_to_x(x,y);
              subplot(3,2,5); plot(mas_otschety_num($-size_fft+1:$,3));
              subplot(3,2,6); plot2d(x(4:size_grafik)*koef_hz,y(4:size_grafik));
//        subplot(3,1,1); plot(fft(mas_otschety_num($-size_fft+1:$,1))(2:$));
//        subplot(3,1,2); plot(fft(mas_otschety_num($-size_fft+1:$,2))(2:$));
//        subplot(3,1,3); plot(fft(mas_otschety_num($-size_fft+1:$,3))(2:$));
//

    end

    drawnow();
endfunction


////Оконное преобразование Фурье, 
//// Time Frequency Toolbox >> Time Frequency Toolbox > Choice of Particular Signals > tftb_window 
//window_size=512;
////h=zeros(window_size,1);
//if getos() == 'Windows' then 
//    getd('C:\scilab-5.5.2\contrib\stftb_1.2.3\macros\')
//end
//h=tftb_window(window_size,'Hamming');
////h(:,:)=1;
//j=max(size(X)); //j=0.5;
//hh=zeros(j*2,window_size);
//for i=1:j;
//    hh(i,:)=h';
//end
//sdvig=window_size/4;
//window_matrix=zeros(window_size/2,(steps-window_size)/sdvig+1);
//for i=1:(steps-window_size)/sdvig+1
////    [yfft1,freq1]=fefft(Zn(:,1+(i-1)*sdvig:window_size+(i-1)*sdvig)'.*hh',...
//    [yfft1,freq1]=fefft(Zn(:,1+(i-1)*sdvig:window_size+(i-1)*sdvig)'.*hh',...
//    Yn(1:window_size));
//    for i1=1:window_size/2
//        window_matrix(i1,i)=max(yfft1(i1,:));
//    end
//end
//mesh([0:steps*dt*sdvig/window_size:steps*dt],[1:window_size/2],window_matrix);
//a=get("current_axes");a.log_flags = "nll";
//title("Оконное БПФ, АЧХ; демпфирование alfa="+string(alfaRelei)+", beta="+string(betaRelei));
//


function y=from_dx_to_x(x1,y1)
    for i=1:max(size(y1)) do
        y(i)=y1(i)/(2*%pi*x1(i));
    end
endfunction

function y=from_ddx_to_x(x1,y1)
    for i=1:max(size(y1)) do
        //y(i)=y1(i)/(2*%pi*x1(i))^2;
    y(i)=y1(i)/x1(i);
    
    end
endfunction

function main_loop_stop()
    global serial_handle
    global doloop
    
    doloop = %F;
    closeserial(serial_handle);
    messagebox('Остановлено')
endfunction

function [x,y]=correct_fft(yN,dt)
  
    //h=tftb_window(max(size(yN)/2),'Hamming'); //окно Хэмминка из модуля Time Frequency Toolbox
    h=window('hm',max(size(yN)/2));
                                                                
    ntime=max(size(yN));
   // dt=(t(1,ntime)-t(1,1))/ntime;
    N=fix(log10(ntime)/log10(2));
    //disp(N)
    yfft=fft(yN(1:2^N));
    yfft=abs(yfft(1:2^N/2))*dt;
    freq0=0;
    freqf=(1/dt)/2;
    df=freqf/(2^N/2);
    freq=0:df:freqf-df;
    x=freq;
    y=yfft;
    //disp(size(h));
    //disp(size(y));
    //y=y.*h';
endfunction


function y=main_loop_start()
 global serial_handle;
 global doloop;
 global mas_otschety_num mas_otschety_size
 global size_max;
 global display_schetcik;
 global size_fft size_obnovl;

 
 
 //clear mas_otschety_num; //очистим массив значений
 mas_otschety_num=[];
 mas_otschety_size=1;

 doloop = %T
 serial_handle=openserial(7,"250000,n,8,1")//"115200,n,8,1"


 while doloop do

 
    mas_all=[]; mas_all_size=1;

    //t=%T;

    while %T do//mas_all_size<size_obnovl do//size_fft do
        [q,s]=serialstatus(serial_handle);
        //disp(q(1));
        if q(1)>0 then 
            break;//t=%F; 
        end;
    end
        //disp(q)
        //while q(1)>0 & mas_all_size<size_obnovl do //size_fft do
    //    if q(1)>0 & mas_all_size<size_obnovl then //size_fft do
            mas_promejut=[];    
            mas_promejut=readserial(serial_handle);
            //if mas_all_size+length(mas_promejut)>size_max then
            //    mas_all=[]; mas_all_size=1;
            //end
            mas_all(mas_all_size)=mas_promejut;//(j); 
            mas_all_size=mas_all_size+length(mas_promejut);//mas_all_size+1; 
            //for j=1:max(size(mas_promejut)) do

            //end  
            //disp(mas_promejut);
    //    end
    //end
    //closeserial(serial_handle);
    
    //if i<size_obnovl then
        L=""; L=strcat(mas_all);
        //disp(L)
        fd=mopen('text_mputl.txt','wt');
        mputl(L,fd);
        mclose(fd);
        
        fd=mopen('text_mputl.txt','rt');
        mas_otschety_text=mgetl(fd,-1);
        mclose(fd);
        
        for i=1:min(size_max,max(size(mas_otschety_text))) do
            [n,p1,p2,p3]=msscanf(mas_otschety_text(i),"%f %f %f");
            if n==3 then
                mas_otschety_num(mas_otschety_size,1:3)=[p1,p2,p3];
                mas_otschety_size=mas_otschety_size+1;
                if mas_otschety_size==size_max then
                    disp("!!!!");
                    mas_otschety_num=[]; 
                    mas_otschety_size=1;
                    display_schetcik=int(size_fft/size_obnovl)-1;
                end
            end  
        end
        
        grafiki_obnovlenie();
    //end
 end
end












okno_upravlenie=gcf(); // создать окно для управления
    okno_upravlenie.figure_size= [610,469]/2;
    okno_upravlenie.figure_name= "Управление FFT";
    okno_upravlenie.figure_position = [63,486];
    //okno_upravlenie.pixel_drawing_mode="xor";//3 - copy, 6 - xor
    okno_upravlenie.anti_aliasing="off"; //2x,4x,8x,16x
anti_aliasing="off"; //2x,4x,8x,16x
okno_grafiki=figure(); // Create a figure
    okno_grafiki.figure_name= "Графики";
    okno_grafiki.figure_position = [709,80]
    //okno_grafiki.pixel_drawing_mode="xor";//3 - copy, 6 - xor
    okno_grafiki.anti_aliasing="off"; //2x,4x,8x,16x
//okno_upravlenie_h=uicontrol(okno_upravlenie,"style","listbox","position", [10 10 150 160]); // Create a listbox
//set(okno_upravlenie_h, "string", "item 1|item 2|item3");// fill the list
//set(okno_upravlenie_h, "value", [1 3]); // select item 1 and 3 in the list

scf(okno_upravlenie); // Make graphic window 0 the current figure
    button_start=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [0 0 0.5 0.5],...
                "string","$Start$",...
                "callback"  , "main_loop_start()");
    //uicontrol(f, "style", "pushbutton", "units", "normalized", "position", [0 0 0.5 0.5], "string", "Button", "horizontalalignment", "center");


    button_stop=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [0.5 0 0.5 0.5],...
                "string","$Stop$",...
                 "callback"  , "main_loop_stop()",...
                 "callback_type", 10);
//    button_stop=uicontrol(okno_upravlenie,...
//                  "style","radiobutton",...
//                  "position", [25,140,60,20],...
//                  "string","Остановка",...
//                  "value",1);



//// Include an editable table into a figure:
//// Building a table of data:
//params = [" " "Country" "Population [Mh]" "Temp.[Â°C]" ];
//towns = ["Mexico" "Paris" "Tokyo" "Singapour"]';
//country = ["Mexico" "France" "Japan" "Singapour"]';
//pop  = string([22.41 11.77 33.41 4.24]');
//temp = string([26 19 22 17]');
//table = [params; [ towns country pop temp ]]
//
//f = gcf();
//clf
//as = f.axes_size;  // [width height]
//ut = uicontrol("style","table",..
//               "string",table,..
//               "position",[5 as(2)-100 300 87],.. // => @top left corner of figure
//               "tooltipstring","Data from majors towns")
//
//// Modify by hand some values in the table. Then get them back from the ui:
//matrix(ut.string,size(table))



// This is a code for Arduino IDE.


#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

const int IN3=A4; //SCL
const int IN4=A5; //SDA

#define DEBUG

MPU6050 mpu;

int16_t ax, ay, az;
int16_t gx, gy, gz;

int a1,a2,a3,a4,a5,a6,i;
double x,y,z;
      // the values of a1,a2 & a3 are for average collibration

void setup() {

   pinMode(IN4,OUTPUT);
   pinMode(IN3,OUTPUT);
  
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        TWBR = 24; 
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
 
    //Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();

// #if defined(DEBUG)
    //Serial.println(F("Testing device connections..."));
    //Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
    //Serial.println("Configuring... Please don't touch the device for 14 seconds");
// #endif

    delay(100);
    a1=0; a2=0; a3=0;
      mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    
    for (i=1; i<=10; i++) {
      delay(100);//1000
      mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
      a1=int((a1+gx)/2); a2=int((a2+gy)/2); a3=int((a3+gz)/2);
      a4=int((a4+ax)/2); a5=int((a5+ay)/2); a6=int((a6+az)/2);
 }
//    a1=int(a1/100); a2=int(a2/100); a3=int(a3/100);
//     a4=int(a4/100); a5=int(a5/100); a6=int(a6/100);

//    accelgyro.setXAccelOffset(a1);
//    accelgyro.setYAccelOffset(a2);
//    accelgyro.setZAccelOffset(a3);


// #if defined(DEBUG)
    //Serial.print("Corrected value are: ");
/*    Serial.print(a1);    Serial.print("\t");
   Serial.print(a2);    Serial.print("\t");
    Serial.print(a3);    Serial.println("\t");
    Serial.print(a4);    Serial.print("\t");
    Serial.print(a5);    Serial.print("\t");
    Serial.print(a6);    Serial.println("\t");
 */   
// #endif

#if defined(DEBUG)
    Serial.begin(250000);//115200);
 #endif

}

void loop() {

  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  //x = map(ax-a1, -17000., 17000., 0., 179.);
  //y = map(ay-a2, -17000., 17000., 0., 179.);
  //z = map(az-a3, -17000., 17000., 0., 179.);
  //Serial.print("\n");
  
 // Serial.print(x);  Serial.print("\t");
 // Serial.print(y);  Serial.print("\t");
 // Serial.print(z);  Serial.print("\t");

//  double g1,g2,g3;
//  g1=(gx);
//  g2=(gy);///1000.;
//  g3=(gz);///1000.;

#if defined(DEBUG)
 Serial.print(ax-a4);  Serial.print(" ");
 Serial.print(ay-a5);  Serial.print(" ");
 Serial.print(az-a6);  Serial.println(" ");
#endif

 //delay(1);
}

